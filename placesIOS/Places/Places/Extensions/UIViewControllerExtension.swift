//
//  UIViewControllerExtension.swift
//  Places
//
//  Created by FOI on 14/10/16.
//  Copyright © 2016 unemployed. All rights reserved.
//

import Foundation
import SVProgressHUD

extension UIViewController {
    func showAlert(title:String, message:String, handler: @escaping ((UIAlertAction)->Void)) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style: .default, handler: handler)
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
     
    func showActionSheet(title:String,actions: [UIAlertAction]) {
        let alertVC = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        for action in actions {
             alertVC.addAction(action)
        }
        self.present(alertVC, animated: true, completion:nil)
    }
    func showAlert(title:String, message: String) {
        let alertVC = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        self.present(alertVC, animated: true, completion:nil)
    }
    
    func showSpinner(){
        SVProgressHUD.show()
    }
    func showSpinnerWithSuccessStatus(_ status:String){
        SVProgressHUD.show(withStatus: status)
    }
    func hideSpinner(){
        SVProgressHUD.dismiss()
    }
    func showSpinnerWithErrorStatus(error: String){
        SVProgressHUD.showError(withStatus: error)
    }
}
