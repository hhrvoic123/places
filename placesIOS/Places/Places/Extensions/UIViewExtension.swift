//
//  Extensions.swift
//  Places
//
//  Created by FOI on 13/10/16.
//  Copyright © 2016 unemployed. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
   
    func roundTheView(){
        self.layer.cornerRadius = self.frame.size.width / 2;
        self.clipsToBounds = true
        self.layer.borderWidth = 1.0
    }
    
}
