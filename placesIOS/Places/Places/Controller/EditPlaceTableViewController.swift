
//
//  EditPlaceViewController.swift
//  Places
//
//  Created by FOI on 13/10/16.
//  Copyright © 2016 unemployed. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import Unbox
class EditPlaceTableViewController: UITableViewController{
    
    
    @IBOutlet weak var placeName : UITextField!
    @IBOutlet weak var placeDetail : UITextField!
    @IBOutlet weak var placeCategory : UIPickerView!
    @IBOutlet weak var buttonAdd : UIButton!
    @IBOutlet weak var placePictureView : UIImageView!
    let placeholderimg = UIImage(named:"plainImageBig")!
    let imagePickerController = UIImagePickerController()
    var annotationView: MKAnnotationView?
    var blurEffectView: UIVisualEffectView?
    var lastImage:UIImage?
    var locationToSave:Location? {
        didSet {
             let pinLocation = annotationView!.annotation as! PinnableLocation
             pinLocation.locationData = locationToSave
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        placeCategory.delegate = self
        placeCategory.dataSource = self
        //buttonAdd.roundTheView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        
        placePictureView.contentMode = .scaleAspectFit
        
        //init for blurview
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView!.frame = self.view.bounds
        
        blurEffectView!.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        //annotationView has to be set before this
        
        if let pin = annotationView?.annotation as? PinnableLocation {
            placeName.text = pin.title!
            placeDetail.text = pin.subtitle!
            if let locdata = pin.locationData {
                locationToSave = locdata
                if let imgid = locdata.imageId {
                    let url = Constants.serverAddressRoot+"/files/"+imgid
                    placePictureView.kf.setImage(with: URL(string:url),placeholder:placeholderimg)
                }
                
            }
            else {
               placePictureView.image = placeholderimg
            }
        }
        lastImage = placePictureView.image
        
    }

    @IBAction func onAddImagePressed(sender: UIButton) {
        presentImagePicker()
    }
   
    func updatePin(){
        let location = annotationView?.annotation as! PinnableLocation
        location.title = placeName.text
        location.subtitle = location.locationData!.details + " - " + location.locationData!.category
        let imageView = UIImageView()
        imageView.frame.origin.x = annotationView!.leftCalloutAccessoryView!.frame.origin.x
        imageView.frame.origin.y = annotationView!.leftCalloutAccessoryView!.frame.origin.y
        imageView.frame.size.width = annotationView!.leftCalloutAccessoryView!.frame.size.width
        imageView.frame.size.height = annotationView!.leftCalloutAccessoryView!.frame.size.height
        imageView.image = placePictureView.image!
        imageView.contentMode = .scaleAspectFit
        annotationView!.leftCalloutAccessoryView = imageView
        annotationView?.image = MapIconHelper.getMapIcon(by:location.locationData!.category, pixels: 40)
        location.locationData = locationToSave
    }
    func addBlur(){
        self.view.addSubview(blurEffectView!)
    }
    func removeBlur(){
        self.blurEffectView!.removeFromSuperview()
    }
    func presentImagePicker(){
        var actions = [UIAlertAction]()
        self.imagePickerController.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            actions.append(UIAlertAction(title: "Camera", style: .default){ _ in
                 self.imagePickerController.sourceType = .camera
                 self.present(self.imagePickerController,animated: true)
            })
        }
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            actions.append(UIAlertAction(title: "Saved photos", style: .default){ _ in
                self.imagePickerController.sourceType = .savedPhotosAlbum
      
                self.present(self.imagePickerController,animated: true)

            })
        }
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            actions.append(UIAlertAction(title: "Photo library", style: .default){ _ in
                self.imagePickerController.sourceType = .photoLibrary
                self.present(self.imagePickerController,animated: true)

            })
        }
        actions.append(UIAlertAction(title: "Cancel", style: .cancel){ _ in
            self.dismiss(animated: true, completion: nil)
        })

        self.showActionSheet(title: "Where do you want to take pictures from", actions: actions)
    }
       func uploadImageToServer(){
       
        let url: URL = URL(string: Constants.serverAddressRoot+"/files")!
        let headers = ["Content-Type":"image/jpg"]
        addBlur()
        let progView:UIProgressView = UIProgressView()
        blurEffectView!.addSubview(progView)
        
        let alert=UIAlertController(title: "Please Wait", message: "Uploading picture...", preferredStyle: UIAlertControllerStyle.alert)
        progView.frame  = CGRect(origin: CGPoint(x:0, y: (view.frame.height/2)+30), size: (CGSize(width: view.frame.width, height: view.frame.height*0.2)))
      
        present(alert, animated: true)
        
        let bytes = placePictureView.image!.jpegData(UIImage.JPEGQuality.highest)
        let req = Alamofire.upload(bytes!, to: url, headers:headers).uploadProgress { progress in // main queue by default
            print("Upload Progress: \(progress.fractionCompleted)")
                progView.setProgress(Float(progress.fractionCompleted), animated: true)
            }
            .responseJSON { response in
                alert.dismiss(animated: true, completion: {_ in
                    
                    self.removeBlur()
                    switch response.result {
                    case .success:
                        
                        do {
                            let imgResp:UploadResponse = try unbox(data:response.data!)
                            self.locationToSave!.imageId = imgResp.id
                            self.uploadLocationToServer()
                        }
                        catch {
                            print("An error occured: \(error)")
                        }
                    case .failure(let error):
                        print(error)
                    }

                })
                }
        debugPrint(req)
    }
    func uploadLocationToServer(){
      
        var urlStr = Constants.serverAddressRoot + "/locations"
        var method:HTTPMethod = .post
        if let currentLocationId = locationToSave!.id { //if location already exists on server you have to update it via put method
            urlStr = urlStr + "/\(currentLocationId)"
            method = .put
        }
        let url: URL = URL(string: urlStr)!
        self.showSpinner()
        let headers = ["Content-Type":"application/json"]
        let params = locationToSave!.toJson()
        let req = Alamofire.request(url, method: method, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON {
            (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success:
                self.hideSpinner()
                do {
                    let locationResp:UploadResponse = try unbox(data:response.data!)
                    self.locationToSave!.id = locationResp.id
                    }
                catch {
                    print("An error occured: \(error)")
                }
                self.showAlert(title: "Sucess", message: "Location saved", handler: {_ in
                     self.navigationController!.popViewController(animated: false)
                })
                
                break
                
            case .failure(let error):
                print(error)
                self.showSpinnerWithErrorStatus(error: "Error happened")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.hideSpinner()
                    self.navigationController!.popViewController(animated: false)
                }
                break
            }
        }
        debugPrint(req) //curl  repr. of request
    }
    func persistToServer() {
        
        //image saving first
        if (!placePictureView.image!.isEqualToImage(image: lastImage!)){//if image is different than last one
            uploadImageToServer()
        }
        else {
            uploadLocationToServer()
        }
        
        
    }
    @IBAction func onSave(_ sender: UIButton) {
      
        
  
        guard let placeName = placeName.text, placeName.characters.count > 0, let detail = placeDetail.text, detail.characters.count > 0 else {
            self.showAlert(title: "Oops, mistake happened.", message: "Please enter all fields", handler: {_ in })
            return
        }
        let location = annotationView?.annotation as! PinnableLocation
        if locationToSave != nil {
           locationToSave!.placeName = placeName
           locationToSave!.details = detail
           locationToSave!.category = Constants.categories[placeCategory.selectedRow(inComponent: 0)]
        }
        else {
            locationToSave = Location(latitude: Float(location.coordinate.latitude), longitude: Float(location.coordinate.longitude), name: placeName, details: detail, category: Constants.categories[placeCategory.selectedRow(inComponent: 0)])
        }
        
        locationToSave?.imageToShow = placePictureView.image
        updatePin()
        persistToServer()
        
    }
}
extension EditPlaceTableViewController: UIPickerViewDataSource, UIPickerViewDelegate {
   
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Constants.categories.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Constants.categories[row]
    }
    
  
}
extension EditPlaceTableViewController: UIImagePickerControllerDelegate,  UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        placePictureView.image = selectedImage
        placePictureView.alpha = 1
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}
