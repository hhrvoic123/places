//
//  FirstViewController.swift
//  Places
//
//  Created by FOI on 11/10/16.
//  Copyright © 2016 unemployed. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import Unbox
import SwiftyJSON
import Kingfisher

class MapsViewController: UIViewController {
    
    @IBOutlet var myMap: MKMapView!
    var lastLocation: CLLocation?
    lazy var locationManager = CLLocationManager()
    var allAnotations:[PinnableLocation] = [PinnableLocation]()
    
  
     @IBAction func onRefresh(sender: UIBarButtonItem) {
        getTheLocations()
    }
    
    var regionSet:Bool = false
   

    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            myMap.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
   
    var blurEffectView: UIVisualEffectView?
    func addBlur(){
        self.view.addSubview(blurEffectView!)
    }
    func removeBlur(){
        self.blurEffectView!.removeFromSuperview()
    }
   
    @IBAction func onCategorize(sender: UIBarButtonItem){
        addBlur()
        let modalVc = self.storyboard?.instantiateViewController(withIdentifier: "modalCategoryPickerController") as! ModalCategoryPickerController
        modalVc.modalPresentationStyle = .overCurrentContext
        modalVc.delegate = self
        present(modalVc, animated: true, completion: nil)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        getTheLocations()
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(MapsViewController.addPin(gestureRecognizer:)))
        longPress.minimumPressDuration = 0.5
        myMap.addGestureRecognizer(longPress)
        checkLocationAuthorizationStatus()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.regular)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView!.frame = self.view.bounds
        blurEffectView!.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation

    }
        func addPin(gestureRecognizer:UIGestureRecognizer) {
        
        if gestureRecognizer.state == UIGestureRecognizerState.began {
            let touchPoint = gestureRecognizer.location(in: myMap)
            let newCoord = myMap.convert(touchPoint, toCoordinateFrom: myMap
            )
            
            CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: newCoord.latitude, longitude: newCoord.longitude)) {
                (placemarks, error) -> Void in
                if let err = error {
                    print("Reverse geocoder failed with error" + err.localizedDescription)
                    return
                }
                var title = "Unknown place"
                var subtitle = "More data not available"
                if (placemarks!.count) > 0 {
                    let pm = placemarks![0]
                    if let throughfare = pm.thoroughfare {
                        title = throughfare
                        if let sublocality = pm.subLocality {
                           subtitle = sublocality
                        }
                    }
                }
                else {
                    print("Problem with the data received from geocoder")
                }
                let annotation = PinnableLocation(coordinate: newCoord,title: title, subtitle: subtitle)
                self.allAnotations.append(annotation)
                self.myMap.addAnnotation(annotation)
            }
        }
    }
}

extension MapsViewController: MKMapViewDelegate {
    
    //defining the display of annotation
    func mapView(_ mapView: MKMapView,
                 viewFor annotation: MKAnnotation) -> MKAnnotationView? {
     
        if let annotation = annotation as? PinnableLocation {
            
            
            var view: MKAnnotationView
            let identifier = "\(annotation.locationData?.longitude)" + "\(annotation.locationData?.longitude)"
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                {
                dequeuedView.annotation = annotation
                view = dequeuedView
                
            } else {
                
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -2, y: 2)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
                
                let imageView = UIImageView (image: UIImage(named:"plainImage"))
                imageView.contentMode = .scaleAspectFit
                view.leftCalloutAccessoryView = imageView
                if let imgid = annotation.locationData?.imageId {
                    let url = Constants.serverAddressRoot+"/files/"+imgid
                
                    imageView.kf.setImage(with: URL(string:url))
                    imageView.frame.origin.x = view.leftCalloutAccessoryView!.frame.origin.x
                    imageView.frame.origin.y = view.leftCalloutAccessoryView!.frame.origin.y
                    imageView.frame.size.width = view.leftCalloutAccessoryView!.frame.size.width
                    imageView.frame.size.height = view.leftCalloutAccessoryView!.frame.size.height
                }
                let img : UIImage!
                if let cat =  annotation.locationData?.category {
                    img = MapIconHelper.getMapIcon(by: cat, pixels: 40)
                }
                else {
                    img = MapIconHelper.getMapIcon(by: "nevermind", pixels: 40)
                }
                view.image = img
            }
            
           
           
            return view
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "editPlaceVC") as! EditPlaceTableViewController
    
        
        vc.annotationView = view //passed by refference (class type)
        
        navigationController?.pushViewController(vc, animated: true)
    }

}
extension MapsViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            
                       let location = locations.last! as CLLocation
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)) 
        
        if let lastBeforeLast = lastLocation {
            
            if (location.distance(from: lastBeforeLast) > 100 || regionSet == false ) {
                self.myMap.setRegion(region, animated: true)
                    if regionSet == false{
                    print("user arrived in new region")
                    regionSet = true
                }
            }
        }
        else {
               self.myMap.setRegion(region, animated: true)
        }
        lastLocation = location
    }
}
extension MapsViewController { //for downloading locations from server
    
    func getTheLocations(){
        let urla = URL(string:Constants.serverAddressRoot+"/locations")!
        let headers = ["Accept":"application/json", "Content-Type": "application/json"]
        self.showSpinner()
        self.allAnotations.removeAll()
        let req = Alamofire.request(urla,encoding: JSONEncoding.default,headers:headers).responseJSON { response in
                   switch response.result {
            case .success:
                do {
                    if let JSON = response.result.value {
                        let locations: [Location] = try unbox(dictionary: JSON as! UnboxableDictionary, atKey: "collection")
                        for location in locations {
                            let annotation = PinnableLocation(coordinate: CLLocationCoordinate2DMake(CLLocationDegrees(location.latitude), CLLocationDegrees(location.longitude)), title: location.placeName, subtitle: location.details+"-"+location.category)
                            annotation.locationData = location
                            self.allAnotations.append(annotation)
                            self.myMap.addAnnotation(annotation)
                        
                        }
                       
                    }
                }
                catch {
                    print("Parsing error: \(error)")
                    self.showSpinnerWithErrorStatus(error: "Error hapened while getting the locations. Contact the developers.")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.hideSpinner()
                        }
                  
                }
            self.hideSpinner()
            case .failure(let error):
                print(error)
                self.showSpinnerWithErrorStatus(error: "Communication error. Please try again later.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.hideSpinner()
                }
               
            }
            
        }
       debugPrint(req)
    }
}

extension MapsViewController:CategoryChooserDelegate {
    func filterMapAnnotations(locationIds:[String]){
            for annotation in myMap.annotations {
                self.myMap.removeAnnotation(annotation)
            }
           let filteredAnnotations = allAnotations.filter{
            if let id = $0.locationData?.id {
                return locationIds.contains(id)
            }
            else {
           return false
          }
        }
         self.myMap.addAnnotations(filteredAnnotations)
    }
    
    func categoryDidChoose(category: String, radius: Int) {
        removeBlur()
        print("Maping by:" + category + ", reducing by category and radius: \(radius)")
        let url = URL(string:Constants.serverAddressRoot+"/locations/"+category+"/\(radius)")!
        let headers = ["Accept":"application/json", "Content-Type": "application/json"]
        let params = ["lat":lastLocation?.coordinate.latitude, "lon": lastLocation?.coordinate.longitude]
        self.showSpinner()
        let req = Alamofire.request(url,method:.post,parameters:params,encoding:JSONEncoding.default,headers:headers).responseData { response in

            switch response.result {
            case .success:
                    if let data = response.result.value {
                        let json = JSON(data: data)
                        if let resp = json["resp"].dictionary,
                            let value = resp["value"]?.dictionary
                            {
                                if let locationIds = value["reducedArr"]?.arrayObject as? [String] {
                                       self.filterMapAnnotations(locationIds: locationIds)
                                }
                                else if let locationId = value["id"]?.string{ //reduce function not called, just one value returned
                                       let stringArr = [locationId]
                                       self.filterMapAnnotations(locationIds: stringArr)
                                }
                        }
                        else {
                            self.showAlert(title: "Oops", message: "Cannot find matching places", handler: {_ in})
                        }
                }
                self.hideSpinner()
            case .failure(let error):
                print(error)
                self.showSpinnerWithErrorStatus(error: "Communication error. Please try again later.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.hideSpinner()
                }
                
            }
            
        }
        debugPrint(req)
    }
    func dismissed() {
        removeBlur()
    }
}
