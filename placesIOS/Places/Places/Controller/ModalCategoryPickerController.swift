//
//  ModalCategoryPickerController.swift
//  Places
//
//  Created by Infinum on 07/01/2017.
//  Copyright © 2017 unemployed. All rights reserved.
//

import UIKit
protocol CategoryChooserDelegate {
    func categoryDidChoose(category:String, radius:Int)
    func dismissed()
}
class ModalCategoryPickerController: UIViewController {
    
    @IBOutlet weak var placeCategory : UIPickerView!
    var delegate:CategoryChooserDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        placeCategory.delegate = self
        placeCategory.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        delegate?.categoryDidChoose(category: Constants.categories[placeCategory.selectedRow(inComponent: 0)], radius: Array(Constants.radiuses.keys) [placeCategory.selectedRow(inComponent: 1)])
        dismiss(animated: true, completion: nil)
    }
    @IBAction func dismissModal(_ sender: UIButton) {
        delegate?.dismissed()
        self.dismiss(animated: true, completion: nil)
        
    }
    
}

extension ModalCategoryPickerController: UIPickerViewDataSource, UIPickerViewDelegate {
    enum PickerViewComponents: Int {
        case placeCategory = 0
        case placeRadius = 1
        
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch(component) {
        case PickerViewComponents.placeCategory.rawValue: return Constants.categories.count
        case PickerViewComponents.placeRadius.rawValue: return Constants.radiuses.count
        default: return 0
        }
        
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch(component) {
        case PickerViewComponents.placeCategory.rawValue:
            return Constants.categories[row]
        case PickerViewComponents.placeRadius.rawValue:
            return  Array(Constants.radiuses.values) [row]
        default: return ""
        }
    }
    
}
