//
//  LocationUploadRepsonse.swift
//  Places
//
//  Created by Infinum on 05/01/2017.
//  Copyright © 2017 unemployed. All rights reserved.
//

import Foundation
import Unbox
struct LocationUploadResponse : Unboxable {
    let imageId: String
    //hardcoded for now
    init(unboxer: Unboxer) throws {
        self.imageId = try unboxer.unbox(key: "_id")
    }
}
