//
//  Constants.swift
//  Places
//
//  Created by FOI on 14/10/16.
//  Copyright © 2016 unemployed. All rights reserved.
//

import Foundation

class Constants {
    public static let categories: [String] = [ "Sculpture", "Monument", "Building", "Park"
        ,"Restaurant", "Caffe-Bar", "Club", "Theater", "Cinema",
          "Shopping" ]
    public static let radiuses: [Int:String] = [ 200:"200m", 2000:"2km", 10000:"10km" ,50000:"50km", 200000:"200km"]
    
    public static var serverAddressRoot: String {
        get {
            let val = TARGET_OS_SIMULATOR == 0 ? "http://192.168.1.107:3000":"http://localhost:3000"
            print (val)
            return val
        }
    }
    
}
