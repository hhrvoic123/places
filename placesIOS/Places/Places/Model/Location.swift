//
//  Location.swift
//  Places
//
//  Created by FOI on 11/10/16.
//  Copyright © 2016 unemployed. All rights reserved.
//

import Unbox

struct Location : Unboxable {//model for locaiton
    var id: String?
    var latitude: Float
    var longitude: Float
    var placeName: String
    var details: String
    var category: String
    var imageId: String?
    var imageToShow: UIImage?
    init(unboxer: Unboxer) throws {
        //latitude =
         latitude = try unboxer.unbox(key: "lat")
         longitude = try unboxer.unbox(key: "long")
        id = unboxer.unbox(key: "_id")
        placeName = try unboxer.unbox(key: "placename")
        details = try unboxer.unbox(key: "details")
        imageId =  unboxer.unbox(key: "imageId")
        category = try unboxer.unbox(key:"category")
    }
    init (latitude: Float, longitude: Float, name:String, details:String, category: String) {//constructor for user saved data
        self.latitude = latitude
        self.longitude = longitude
        self.placeName = name
        self.details = details
        self.category = category
    }
    func toJson() -> [String:Any] {
        var jsonData =
        ["placename":placeName,
         "lat" : latitude,
         "long" : longitude,
         "details" : details,
         "category" : category,
        ] as [String : Any]
        //check the nils and add them if needed
        if let id = self.id {
            jsonData["_id"] = id
        }
        if let imgId = self.imageId {
            jsonData["imageId"] = imgId
        }
        return jsonData
    }
    
}
