//
//  ImageUploadResponse.swift
//  Places
//
//  Created by Infinum on 05/01/2017.
//  Copyright © 2017 unemployed. All rights reserved.
//

import Foundation
import Unbox
struct UploadResponse : Unboxable { //api always returns _id (mongo id in db)
    let id: String

    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "_id")
    }
}
