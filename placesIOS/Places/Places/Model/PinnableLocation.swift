//
//  PinnableLocation.swift
//  Places
//
//  Created by FOI on 12/10/16.
//  Copyright © 2016 unemployed. All rights reserved.
//

import Foundation
import MapKit

class PinnableLocation: NSObject, MKAnnotation {
    var locationData: Location? //that will be saved on server (user data)
//       /** This property starts out YES until modified manually or loaded from the network. This way dragging the pin will update the coordinates and geocoded info */
    var isConfiguredBySystem = false
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var pinCustomImageName:String!
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
    
    func pinTintColor() -> UIColor {
        if let userData = locationData {
            switch userData.category{
            case "Sculpture", "Monument", "Building", "Park":
                return MKPinAnnotationView.redPinColor()
            case "Food restaurant", "Caffe-Bar", "Club", "Theater", "Cinema" :
                return MKPinAnnotationView.purplePinColor()
            case "Shop centre", "Shop":
                return UIColor.cyan
            default:
                return MKPinAnnotationView.greenPinColor()
        }
        }
         return MKPinAnnotationView.greenPinColor()
    }
}
