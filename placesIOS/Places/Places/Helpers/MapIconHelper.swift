//
//  MapIconHelper.swift
//  Places
//
//  Created by Infinum on 11/01/2017.
//  Copyright © 2017 unemployed. All rights reserved.
//

import Foundation
import UIKit
class MapIconHelper {

    static func getMapIcon(by category:String, pixels: Int) -> UIImage {
        let img : UIImage!
        
       
            switch category {
            case "Sculpture":
                fallthrough
            case "Monument":
                fallthrough
            case "Building" :
                img = UIImage(named:"Monument")
            case "Shopping ":
                img = UIImage(named:"cart")
            case "Restaurant":
                img = UIImage(named:"restaurant")
            case "Caffe-Bar":
                img = UIImage(named:"coffee")
            case "Club":
                img = UIImage(named:"club")
            case "Theater":
                fallthrough
            case "Cinema":
                img = UIImage(named:"cinema")
            case "Park":
                img = UIImage(named:"park")
            default:
                img = UIImage(named:"location")
            }
        let resizedImage = img.resizeWith(width: CGFloat(pixels))
        return resizedImage!
    }
}
